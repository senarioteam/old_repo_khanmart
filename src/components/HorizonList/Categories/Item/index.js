/** @format */

import React, { Component } from "react";
import { Text, View, Image } from "react-native";

import { withTheme } from "@common";
import { TouchableScale } from "@components";
import Reactotron from 'reactotron-react-native'

const defaultImage = require('../../../../images/categories_icon/ic_seeds.png')

import styles from "./styles";

class Item extends Component {
  render() {
    const {
      item,
      label,
      onPress,
      image,
      theme: {
        colors: { text },
      },
    } = this.props;
    return (
      <View style={styles.container}>
        <TouchableScale
          scaleTo={0.7}
          style={styles.wrap}
          onPress={() => onPress({ ...item, circle: true, name: label })}>
          <View
            style={[
              styles.background,
              { opacity: 0.08, backgroundColor: '#3b5998' },
            ]}
          />

          <View style={styles.iconView}>
          {item.image === null ? (

            <Image
               source={image}
              style={[styles.icon,{tintColor:"transprent"}]}
            />
          ):(
            <Image
               source={{uri:image}}
              style={[styles.icon,{tintColor:"transprent"}]}
            />
          )
          }
          </View>
          <Text  numberOfLines={1} style={[styles.title, { color: text }]}>{label}</Text>
        </TouchableScale>
      </View>
    );
  }
}

export default withTheme(Item);
