/** @format */

import React from "react";
import { ScrollView, Button } from "react-native";
import { connect } from "react-redux";

import { withTheme, Languages  } from "@common";
import Footer from "./Footer";
import ShippingAddress from "./ShippingAddress";
import LineItemsAndPrice from "./LineItemsAndPrice";
import OrderStatus from "./OrderStatus";
import OrderNotes from "./OrderNotes";
import styles from "./styles";
import { Reactotron } from "@app/Omni";

class OrderDetail extends React.PureComponent {
  componentDidMount() {
    this.props.getOrderNotes(this.props.order.id);
  }
  _onPressTracking = async () => {
      const {navigation} = this.props;
          navigation.navigate("OrderTrackingScreen", {
            orderId: this.props.order.id
          })
    };

  render() {
    const { order, theme, orderNotes } = this.props;

    return (
      <ScrollView
        style={styles.container(theme.colors.background)}
        contentContainerStyle={styles.contentContainer}>
        <LineItemsAndPrice order={order} theme={theme} />
        <OrderStatus order={order} theme={theme} />
        <ShippingAddress shipping={order.shipping} theme={theme} />
        <OrderNotes orderNotes={orderNotes} theme={theme} />
        <Button
          title="Tracking"
          style={[styles.button, {marginBottom: 2}]}
          // textStyle={styles.buttonText}
          onPress={() => this._onPressTracking()}
          // isLoading={refunded}
        />
        {/* {order.status !== "cancelled" && order.status !== "refunded" && (
          <Footer order={order} />
        )} */}
      </ScrollView>
    );
  }
}

const mapStateToProps = ({ carts }, ownProps) => {
  const order = carts.myOrders.find((o, i) => o.id === ownProps.id);

  return { carts, order, orderNotes: carts.orderNotes };
};

function mergeProps(stateProps, dispatchProps, ownProps) {
  const { dispatch } = dispatchProps;
  const { actions } = require("@redux/CartRedux");
  return {
    ...ownProps,
    ...stateProps,
    getOrderNotes: (orderId) => {
      actions.getOrderNotes(dispatch, orderId);
    },
  };
}

export default connect(
  mapStateToProps,
  null,
  mergeProps
)(withTheme(OrderDetail));
