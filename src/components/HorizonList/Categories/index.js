/** @format */

import React, { PureComponent } from "react";
import { FlatList, Text, View } from "react-native";
import Reactotron from "reactotron-react-native";


const defaultImage = require('../../../images/categories_icon/app.png');

import { withTheme } from "@common";

import Item from "./Item";
import { categories } from "@app/selectors/CategorySelector";

class Categories extends PureComponent {
  static defaultProps = {
    items: [],
  };

  render() {
    const { items, type, onPress, config, categories } = this.props;

    const column = typeof config.column !== "undefined" ? config.column : 1;

    return (
      <FlatList
        keyExtractor={(item, index) => `${index}`}
        contentContainerStyle={styles.flatlist}
        showsHorizontalScrollIndicator={false}
        horizontal={column === 1}
        numColumns={column}
        data={categories}
        renderItem={({ item, index }) => {
          return (
            <Item
              key={index}
              item={item}
              type={type}
              label={item.name}
              image={item.image === null ? defaultImage: item.image.src}
              onPress={onPress}
            />
          );
        }}
      />
    );
  }
}

const styles = {
  flatlist: {
    marginBottom: 10,
  },
};
// const mapping = (state, props) => ({
//   getCategories: categories(state, props),
// });

export default withTheme(Categories);

// export default connect(mapping)(withTheme(Categories));
