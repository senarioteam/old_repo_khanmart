/** @format */

import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import { View, Text, Alert } from "react-native";

import { Color, Styles, withTheme } from "@common";
import { OrderDetail } from "@containers";
import { Logo, Back } from "./IconNav";

import MapView, { PROVIDER_GOOGLE } from "react-native-maps";
import firebase from "firebase";
//import firebase from '@react-native-firebase/app';
import Reactotron from "reactotron-react-native";

@withTheme
export default class OrderTrackingScreen extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      OrderID: "",
      isLoading: false,
      dbLocation: {
        longitude: 74.3043166,
        latitude: 31.4674071,
        latitudeDelta: 0.0922,
        longitudeDelta: 0.0421,
      },
    };
    //   this.flatlistRef = React.createRef();
  }

  static navigationOptions = ({ navigation }) => {
    const dark = navigation.getParam("dark", false);
    const headerStyle = navigation.getParam(
      "headerStyle",
      Styles.Common.toolbar()
    );

    return {
      headerTitle: Logo(),
      tabBarVisible: false,
      headerLeft: Back(navigation, null, dark),

      headerTintColor: Color.headerTintColor,
      headerStyle,
      headerTitleStyle: Styles.Common.headerStyle,
    };
  };

  static propTypes = {
    navigation: PropTypes.object.isRequired,
  };

  UNSAFE_componentWillMount() {
    const {
      theme: {
        colors: { background },
        dark,
      },
    } = this.props;

    this.props.navigation.setParams({
      headerStyle: Styles.Common.toolbar(background, dark),
      dark,
    });
    this.setState(
      {
        OrderID: this.props.navigation.state.params.orderId,
      },
      function () {}
    );
    this.firebaseSettings();

    Reactotron.log("PARAMS : ", this.props.navigation.state.params.orderId);
  }

  componentDidMount() {
    this.readUserDataonChnages();
  }

  firebaseSettings = () => {
    var firebaseConfig = {
      apiKey: "AIzaSyBpWvPTl00ppoJYiMC5jOdwHie_OyecQLU",
      authDomain: "khanmart-dd4d0.firebaseapp.com",
      databaseURL: "https://khanmart-dd4d0.firebaseio.com",
      projectId: "khanmart-dd4d0",
      storageBucket: "khanmart-dd4d0.appspot.com",
      messagingSenderId: "32236490539",
      appId: "1:32236490539:web:f991f43c1fa4e8883d3f6e",
      measurementId: "G-1K886TN0J1",
    };

    // Initialize Firebase
    if (!firebase.apps.length) {
      firebase.initializeApp(firebaseConfig);
    }
    // firebase.initializeApp(firebaseConfig);
  };

  readUserDataonChnages = () => {
    const { navigation } = this.props;
    const { dbLocation } = this.state;
    const OrderID = navigation.state.params.orderId;
    const that = this;

    // let dbReference = 'Tracking/' + OrderID + '/';
    let dbReference = "Tracking/" + OrderID + "/";
    firebase
      .database()
      .ref(dbReference)
      .on("value", function (snapshot) {
        let dbData = snapshot.val();
        if (dbData != null && dbData !== undefined) {
          let dbItems = Object.values(dbData);
          if (dbItems.length > 0) {
            let locationData = dbItems[dbItems.length - 1];
            Reactotron.log('[][[][][======', locationData)
            if (
              locationData.latitude != null &&
              locationData.latitude != undefined &&
              locationData.longitude != null &&
              locationData.longitude != undefined
            ) {
              that.setState({
                dbLocation: {
                  longitude: parseFloat(dbItems[dbItems.length - 1].Longitude),
                  latitude: parseFloat(dbItems[dbItems.length - 1].Latitude),
                  // latitudeDelta: 0.0922,
                  // longitudeDelta: 0.0421,
                  //  latitude: 31.4674071,
                  //  longitude: 74.3043166,
                  latitudeDelta: 0.092,
                  longitudeDelta: 0.092,
                },
              });
            }
          }
        } else {
          Alert.alert(
            "Alert",
            "Shipping Tracking is not Started",
            [{ text: "OK", onPress: () => navigation.goBack() }],
            { cancelable: false }
          );
        }
      });
  };

  componentWillReceiveProps(nextProps) {
    if (this.props.theme.dark !== nextProps.theme.dark) {
      const {
        theme: {
          colors: { background },
          dark,
        },
      } = nextProps;
      this.props.navigation.setParams({
        headerStyle: Styles.Common.toolbar(background, dark),
        dark,
      });
    }
  }

  getInitialState() {
    return {
      region: {
        latitude: 37.78825,
        longitude: -122.4324,
        latitudeDelta: 0.0922,
        longitudeDelta: 0.0421,
      },
    };
  }

  onRegionChange(region) {
    this.setState({ region });
  }
  onMapReady = () => {
    this.setState({ isMapReady: true });
  };

  render() {
    const { navigation } = this.props;
    const id = navigation.getParam("id", null);
    // alert(this.state.dbLocation.latitude)

    // if (!id) return null;

    return (
      <MapView
        region={this.state.dbLocation}
        onLayout={this.onMapReady}
        // onRegionChange={this.onRegionChange}
        style={{ flex: 1 }}
        provider={PROVIDER_GOOGLE}
        showsUserLocation={true}
        followsUserLocation={true}
        initialRegion={this.state.dbLocation}
        onRegionChange={(region) => this.setState({ region })}
        onRegionChangeComplete={(region) => this.setState({ region })}
        mapType="standard"
        showsCompass={true}
        rotateEnabled={true}
      >
        <MapView.Marker draggable={true} coordinate={this.state.dbLocation} title="location tracking"/>
      </MapView>
    ); // https://codeburst.io/react-native-google-map-with-react-native-maps-572e3d3eee14
  }
}
